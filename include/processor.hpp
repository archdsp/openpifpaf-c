#include "Python.h"
#include "numpy/arrayobject.h"
#include "pifpaf.hpp"

namespace openpifpaf
{
	class DummyPool
	{
		static void starmap(void);
	};
		
	class Processor
	{
	private:
	public:
		Processor(
			// model, decode
			float keypoint_threshold=0.0,
			float instance_threshold=0.0,
			// device, worker_pool
			float suppressed_v=0.0
			// instance_scorer
			);

		~Processor();
	};
}
