#include <vector>
#include <tuple>
#include <algorithm>

int Ndto1d(const int ndim, const int* iVector, const int* ndims);

namespace openpifpaf
{
	// void index_field(std::tuple<int, int. int> shape);
	void NormalizePif(std::vector<std::tuple<float*, int, int*>> _pif_fields);
	void NormalizePaf(std::vector<std::tuple<float*, int, int*>> _paf_fields);
	// void normalize_paf(void);
	// void ScalarSquareAddSingle(void);
}
