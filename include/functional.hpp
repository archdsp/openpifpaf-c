#include <iostream>

namespace openpifpaf
{
	float Clip(float n, float lower, float upper);
	float ScalarValueClipped(float* field, int row, int col, float x, float y);
}

