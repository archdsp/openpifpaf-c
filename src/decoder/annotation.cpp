#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <tuple>

#include <Python.h>
#include <numpy/arrayobject.h>

#include "functional.hpp"
#include "annotation.hpp"

namespace openpifpaf
{
	/* Constructor */
	Annotation::Annotation(
		std::vector<std::string> keypoints,
		std::vector<std::tuple<int, int>> skeleton)
	{
		_keypoints = keypoints;
		_skeleton = skeleton;
		
		_data = new Matrix<float>(keypoints.size(), 3, 0.0f);
		
		// TODO : Implement initialization of below
		// _joint_scales;
		// _fixed_score;
		// _decoding_order;
		
		for (auto i = _skeleton.begin(); i != _skeleton.end(); i++)
		{
			std::tuple<int, int> tmp = *i;
			_skeleton_m1.push_back(std::make_tuple(std::get<0>(tmp) - 1, std::get<1>(tmp) - 1));
		}
		_score_weights = new Matrix<float>(keypoints.size(), 1, 1.0f);
		(*_score_weights)(0, 0) = 3.0f;
		(*_score_weights)(1, 0) = 3.0f;
		(*_score_weights)(2, 0) = 3.0f;
		_score_weights->Divide(_score_weights->Sum());		
	}

	/* Destructor*/
	Annotation::~Annotation()
	{
		for (int i = _decoding_order.size() - 1 ; i >= 0; i--)
		{
			delete[] std::get<2>(_decoding_order.at(i));
			delete[] std::get<3>(_decoding_order.at(i));
		}
	}

	void Annotation::PrintVars(void)
	{
	}

	Annotation* Annotation::Add(int joint_i, float* xyv, int xyvLen)
	{
		std::copy(&xyv[0], &xyv[0] + xyvLen, &((*_data)(joint_i, 0)));
		return this;
	}

	void Annotation::Frontier()
	{
		std::vector<std::tuple<float, int, bool, int, int>> list1, list2;
		for (unsigned int connection_i = 0; connection_i < _skeleton_m1.size(); connection_i++)
		{
			std::tuple<int, int> tmp = _skeleton_m1.at(connection_i);
			int j1i = std::get<0>(tmp);
			int j2i = std::get<1>(tmp);
			
			if ((*_data)(j1i, 2) > 0.0 && (*_data)(j2i, 2) == 0.0)
				list1.push_back(std::make_tuple((*_data)(j1i, 2), connection_i, true, j1i, j2i));
		}

		for (unsigned int connection_i = 0; connection_i < _skeleton_m1.size(); connection_i++)
		{
			std::tuple<int, int> tmp = _skeleton_m1.at(connection_i);
			int j1i = std::get<0>(tmp);
			int j2i = std::get<1>(tmp);
			
			if ((*_data)(j2i, 2) > 0.0 && (*_data)(j1i, 2) == 0.0)
				list2.push_back(std::make_tuple((*_data)(j2i, 2), connection_i, false, j1i, j2i));
		}	
			
		list1.insert(list1.end(), list2.begin(), list2.end());

		// sort Tuple items in reverse order
		sort(list1.begin(), list1.end(), [](std::tuple<float, int, bool, int, int> first, std::tuple<float, int, bool, int, int> second)->bool {
						 if (std::get<0>(first) == std::get<0>(second))
							 return std::get<1>(first) > std::get<1>(second);
						 else
							 return std::get<0>(first) > std::get<0>(second);
						 
						 if (std::get<1>(first) == std::get<1>(second))
							 return std::get<2>(first) > std::get<2>(second);
						 else
							 return std::get<1>(first) > std::get<1>(second);
						 
						 if (std::get<2>(first) == std::get<2>(second))
							 return std::get<3>(first) > std::get<3>(second);
						 else
							 return std::get<2>(first) > std::get<2>(second);
						 
						 if (std::get<3>(first) == std::get<3>(second))
							 return std::get<4>(first) > std::get<4>(second);
						 else
							 return std::get<3>(first) > std::get<3>(second);

						 if (std::get<4>(first) == std::get<4>(second))
							 return true;
						 else
							 return std::get<4>(first) > std::get<4>(second);

					 });

		_frontier.clear(); 
		_frontier = list1;

		// for (unsigned int i = 0; i < _frontier.size(); i++)
		// {
		// 	std::tuple<float, int, bool, int, int> tmp = _frontier.at(i);
		// 	printf("(%f %d %s %d %d) ", std::get<0>(tmp), std::get<1>(tmp), std::get<2>(tmp) ? "True" : "False", std::get<3>(tmp), std::get<4>(tmp));
		// }
		// printf("c++\n");
	
		return;
	}

    std::tuple<float, int, bool, int, int> Annotation::FrontierNext(bool updateFrontier)
	{
		if (updateFrontier)
            Frontier();

		std::tuple<float, int, bool, int, int> nextItem;
		while (_frontier.size() > 0)
		{
			std::tuple<float, int, bool, int, int> nextItem = _frontier.at(0); _frontier.erase(_frontier.begin()); // vector.popfront
            bool forward = std::get<2>(nextItem);
            int idxTarget = forward ? std::get<4>(nextItem) : std::get<3>(nextItem);
            if ((*_data)(idxTarget, 2) != 0.0)
                continue;

            return nextItem;
		}
		return nextItem;
	}
		
} // end namespace openpifpaf

extern "C"
{
	/* Wrapper Function */
	openpifpaf::Annotation* Annotation_New(PyObject* keypoints,
										   PyObject* skeleton)
	{
		if (!PyList_Check(keypoints) || !PyList_Check(skeleton))
		{
			std::cout << "Parameter must be a list or sub type of list" << std::endl;
			return NULL;
		}
		
		Py_ssize_t keypointsLen = PyList_Size(keypoints);
		Py_ssize_t skeletonLen = PyList_Size(skeleton);

		std::vector<std::string> _keypoints((int)keypointsLen);
		std::vector<std::tuple<int, int>> _skeleton((int)skeletonLen);

		for (int i = 0; i < keypointsLen; i++)
		{
			PyObject* item = PyList_GetItem(keypoints, i);

			// Check if item is string
			if (PyUnicode_Check(item))
			{
				PyObject* pyString = PyUnicode_AsEncodedString(item, "utf-8", "UNICODE STRING ITEM");
				const char *tmpCstChar =  PyBytes_AS_STRING(pyString);
				_keypoints.at(i) = std::string(tmpCstChar);
			}
		}

		for (int i = 0; i < skeletonLen; i++)
		{
			PyObject* item = PyList_GetItem(skeleton, i);

			if (PyTuple_Check(item))
			{
				const int tmpCstInt1 = (int)PyLong_AsLong(PyTuple_GetItem(item, 0));
				const int tmpCstInt2 = (int)PyLong_AsLong(PyTuple_GetItem(item, 1));
				_skeleton.at(i) = std::make_tuple(tmpCstInt1, tmpCstInt2);
			}
		}

		openpifpaf::Annotation* ret = new openpifpaf::Annotation(_keypoints, _skeleton);
		return ret;
	}

	void Annotation_PrintVars(openpifpaf::Annotation* ann)
	{
		ann->PrintVars();
	}

	openpifpaf::Annotation* Annotation_Add(openpifpaf::Annotation* ann, int joint_i, PyObject* xyv)
	{
		if (PyTuple_Check(xyv))
		{
			int xyvLen = (int)PyTuple_Size(xyv);
			float* xyvtmp = new float[xyvLen];
			PyArg_ParseTuple(xyv, "fff", &xyvtmp[0], &xyvtmp[1], &xyvtmp[2]);
			return ann->Add(joint_i, &xyvtmp[0], xyvLen);
		}

		return NULL;
	}

	PyObject* Annotation_FrontierNext(openpifpaf::Annotation* ann, bool updateFrontier)
	{
		std::tuple<float, int, bool, int, int> nextItem = ann->FrontierNext(updateFrontier);
		PyObject* nextList
			= Py_BuildValue("(fiOii)",
							std::get<0>(nextItem),
							std::get<1>(nextItem),
							std::get<2>(nextItem) ? Py_True : Py_False,
							std::get<3>(nextItem),
							std::get<4>(nextItem));
		return nextList;
	}

	float Annotation_GetData(openpifpaf::Annotation* ann, int row, int col)
	{		
		return (*ann->_data)(row, col);
	}

	PyObject* Annotation_GetDataArray(openpifpaf::Annotation* ann)
	{
		int dimension = 2;
		npy_intp dims[] = {ann->_data->Row(), ann->_data->Col()};
		return PyArray_SimpleNewFromData(dimension, dims, NPY_FLOAT, (void*)ann->_data);
	}


	PyObject* Annotation_GetDataRow(openpifpaf::Annotation* ann, int row)
	{		
		PyObject* dataRow = Py_BuildValue("(fff)", (*ann->_data)(row, 0), (*ann->_data)(row, 1), (*ann->_data)(row, 2));
		return dataRow;
	}

	void Annotation_AppendDecodingOrder(openpifpaf::Annotation* ann, int joint1Idx, int joint2Idx, PyObject* joint1, PyObject* joint2)
	{
		if (PyTuple_Check(joint1) && PyTuple_Check(joint2))
		{
			float *joint1TmpXyv = new float[3];
			float *joint2TmpXyv = new float[3];
			if(!PyArg_ParseTuple(joint1, "fff", &joint1TmpXyv[0], &joint1TmpXyv[1], &joint1TmpXyv[2])
			   || !PyArg_ParseTuple(joint2, "fff", &joint2TmpXyv[0], &joint2TmpXyv[1], &joint2TmpXyv[2]))
			{
				delete[] joint1TmpXyv;
				delete[] joint2TmpXyv;
				return;
			}
			
			ann->_decoding_order.push_back(std::make_tuple(joint1Idx, joint2Idx, joint1TmpXyv, joint2TmpXyv));
			// std::tuple<int, int, float*, float*> tmp = ann->_decoding_order[ann->_decoding_order.size() - 1];
			// printf("decoding order %d %d (%f %f %f), (%f %f %f)\n",
			// 	   std::get<0>(tmp), std::get<1>(tmp),
			// 	   std::get<2>(tmp)[0], std::get<2>(tmp)[1], std::get<2>(tmp)[2],
			// 	   std::get<3>(tmp)[0], std::get<3>(tmp)[1], std::get<3>(tmp)[2]);
		}
		return;
	}

	void Annotation_FillJointScales(openpifpaf::Annotation* ann, float* scales, int scalesDimension, PyObject* scalesShape, float hr_scale=1.0f)
	{
		int jointLen = 0, row = 0, col = 0;
		PyArg_ParseTuple(scalesShape, "iii", &jointLen, &row, &col);
		
		ann->_joint_scales = new Matrix<float>(ann->_data->Row(), 1, 0.0f);
		// printf("%f\n", *(xyv_i*col*row + col*423 + 193));

		for (int xyv_i = 0; xyv_i < ann->_data->Row(); xyv_i++)
		{
			if ((*ann->_data)(xyv_i, 2) == 0.0)
				continue;

			int x = (int)openpifpaf::Clip((*ann->_data)(xyv_i, 0) * hr_scale, 0.0, col - 1);
			int y = (int)openpifpaf::Clip((*ann->_data)(xyv_i, 1) * hr_scale, 0.0, row - 1);
			// float scale = openpifpaf::ScalarValueClipped(&scales[xyv_i*row*col],
			// 											 row, col,
			// 											 (*ann->_data)(xyv_i, 0) * hr_scale,
			// 											 (*ann->_data)(xyv_i, 1) * hr_scale);
			float scale = *(scales + xyv_i*col*row + col*y + x);
            (*ann->_joint_scales)(xyv_i, 0) = scale / hr_scale;			
			// printf("%f\n", (*ann->_joint_scales)(xyv_i, 0));
		}

		return;
	}

} // END EXTERN "C"
