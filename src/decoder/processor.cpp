#include <iostream>
#include "torch/torch.h"
#include "processor.hpp"
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION 
namespace openpifpaf
{
	Processor::Processor(float keypoint_threshold,
						 float instance_threshold,
						 float suppressed_v)
	{
		
	}
}

extern "C"
{
	// openpifpaf::Processor* Processor_New(void)
	// {
	// 	return new openpifpaf::Processor();
	// }

	// openpifpaf::Processor* Processor_Free(openpifpaf::Processor* obj)
	// {
	// 	obj->~Processor();
	// 	delete obj;
	// }

	void Processor_KeypointSets(openpifpaf::PifPaf* decoder, PyObject* fields)
	{
		Py_Initialize();
		_import_array();

		if (!PyList_Check(fields))
			return;

		size_t outerListSize = (size_t)PyList_Size(fields);
		if (outerListSize <= 0)
			return;

		std::vector<std::vector<std::tuple<float*, int, int*>>> _fields(outerListSize);
		for (size_t i = 0; i < outerListSize; i++)
		{
			PyObject* innerList = PyList_GetItem(fields, i);
			if (!PyList_Check(innerList))
				return;

			size_t innerListSize = (size_t)PyList_Size(innerList);
			std::vector<std::tuple<float*, int, int*>> _inner_fields(innerListSize);
	
			// Inner NPY Array
			for (size_t j = 0; j < innerListSize; j++)
			{
				PyObject* tmpPyObj = PyList_GetItem(innerList, j);
				if (!PyArray_Check(tmpPyObj))
					return;

				//PyArrayObject* pyArrTmpObj = reinterpret_cast<PyArrayObject*>(tmpPyObj);
				PyArrayObject* pyArrTmpObj = reinterpret_cast<PyArrayObject*>(PyArray_ContiguousFromObject(tmpPyObj, NPY_FLOAT, 0, 0));
				
				int ndim = PyArray_NDIM(pyArrTmpObj);
				// printf("CPP %d,%d ", i, j);
				int* dims = new int[ndim];

				for (int dim_i = 0; dim_i < ndim; dim_i++)
				{
					dims[dim_i] = PyArray_DIM(pyArrTmpObj, dim_i);
				    // printf("%d ", dims[dim_i]);
				}
				// printf("\n");
				
				float* npyData = reinterpret_cast<float*>(PyArray_DATA(pyArrTmpObj));
			// 	// int coord[3] = {16, 62, 62};
			// 	// int ndims[3] = {17, 63, 63};
			// 	// int idx = Ndto1d(3, coord, ndims);
				_inner_fields.at(j) = std::make_tuple(npyData, ndim, dims);
			}
			_fields.at(i) = _inner_fields;
			// printf("\n");
		}

		std::vector<openpifpaf::Annotation> annotations = decoder->Decode(_fields);
        // annotations = self.soft_nms(annotations)

        // for ann in annotations:
        //     kps = ann.data
        //     kps[kps[:, 2] < self.keypoint_threshold] = 0.0
        // annotations = [ann for ann in annotations if ann.score() >= self.instance_threshold]
        // annotations = sorted(annotations, key=lambda a: -a.score())

        // keypoint_sets = [ann.data for ann in annotations]
        // scores = [ann.score() for ann in annotations]
             
        // KEYPOINT_sets = np.array(keypoint_sets)
        // scores = np.array(scores)

		// for (size_t i = 0; i < outerListSize; i++)
		// {
		// 	std::vector<std::tuple<float*, int, int*>> _inner_fields = _fields.at(i);
		// 	for (size_t j = 0; j < _inner_fields.size(); j++)
		// 	{
		// 		std::tuple<float*, int, int*> tmp = _inner_fields.at(j);
		// 		delete[] std::get<0>(tmp);
		// 		delete[] std::get<2>(tmp);
		// 	}			
		// }
		// Py_Finalize();
	}
} // end namespace
