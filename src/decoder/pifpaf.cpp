#include "pifpaf.hpp"
#include "Python.h"

namespace openpifpaf
{
	std::string openpifpaf::PifPaf::connectionMethod = "blend";
    float openpifpaf::PifPaf::fixed_b = 0.5;
    // pif_fixed_scale = None
    float openpifpaf::PifPaf::pafThreshold = 0.1;

	PifPaf::PifPaf(std::vector<int> strides,
				   std::vector<std::string> keypoints,
				   std::vector<std::tuple<int, int>> skeleton,
				   std::vector<int> pifIndices,
				   std::vector<int> pafIndices,
				   std::vector<float> pifMinScales,
				   std::vector<float> pafMinDistances,
				   std::vector<float> pafMaxDistances,
				   float seedThreshold)
	{
		m_Strides = strides;
		m_Keypoints = keypoints;
		m_Skeleton = skeleton;
		m_PifIndices = pifIndices;
		m_PafIndices = pafIndices;
		m_PifMinScales = pifMinScales;
		m_PafMinDistances = pafMinDistances;
		m_PafMaxDistances = pafMaxDistances;
		m_SeedThreshold = seedThreshold;
	}

	std::vector<Annotation> PifPaf::Decode(std::vector<std::vector<std::tuple<float*, int, int*>>> fields)
	{
		std::vector<Annotation> annotations;

		for (size_t i = 0; i < m_PifIndices.size(); i++)
		{
			std::vector<std::tuple<float*, int, int*>> tmp = fields.at(i);
			openpifpaf::NormalizePif(tmp);
		}
		
		// # normalize
        // normalized_pifs = [normalize_pif(*fields[pif_i], fixed_scale=self.pif_fixed_scale) for pif_i in self.pif_indices]
        // normalized_pafs = [normalize_paf(*fields[paf_i], fixed_b=self.fixed_b) for paf_i in self.paf_indices]

        // # pif hr
        // pifhr = PifHr(self.pif_nn)
        // pifhr.fill_sequence(normalized_pifs, self.strides, self.pif_min_scales)

        // # seeds
        // seeds = PifSeeds(pifhr.target_accumulator, self.seed_threshold)
        // seeds.fill_sequence(normalized_pifs, self.strides, self.pif_min_scales)

        // # paf_scored
        // paf_scored = PafScored(pifhr.targets, self.skeleton, score_th=self.paf_th)
        // paf_scored.fill_sequence(normalized_pafs, self.strides, self.paf_min_distances, self.paf_max_distances)

        // gen = generator.Greedy(
        //     pifhr, paf_scored, seeds,
        //     seed_threshold=self.seed_threshold,
        //     connection_method=self.connection_method,
        //     paf_nn=self.paf_nn,
        //     paf_th=self.paf_th,
        //     keypoints=self.keypoints,
        //     skeleton=self.skeleton)

        // print('pifpaf.py에서 generator.greedy.annotation 함수로 콜링')
        // annotations = gen.annotations(initial_annotations=initial_annotations)

		return annotations;
	}
}

extern "C"
{
	openpifpaf::PifPaf* PifPaf_New(PyObject* strides,
								   PyObject* keypoints,
								   PyObject* skeleton,
								   PyObject* pifIndices,
								   PyObject* pafIndices,
								   PyObject* pifMinScales,
								   PyObject* pafMinDistances,
								   PyObject* pafMaxDistances)
	{
		if (!PyList_Check(strides)
			|| !PyList_Check(keypoints)
			|| !PyList_Check(skeleton)
			|| !PyList_Check(pifIndices)
			|| !PyList_Check(pafIndices)
			|| !PyList_Check(pifMinScales)
			|| !PyList_Check(pafMinDistances)
			|| !PyList_Check(pafMaxDistances))
		{
			std::cerr << "Parameter must be a list or sub type of list" << std::endl;
			return NULL;
		}

		size_t stridesLen = (size_t)PyList_Size(strides);
		size_t keypointsLen = (size_t)PyList_Size(keypoints);
		size_t skeletonLen = (size_t)PyList_Size(skeleton);
		size_t pifIndicesLen = (size_t)PyList_Size(pifIndices);
		size_t pafIndicesLen = (size_t)PyList_Size(pafIndices);
		size_t pifMinScalesLen = (size_t)PyList_Size(pifMinScales);
		size_t pafMinDistancesLen = (size_t)PyList_Size(pafMinDistances);
		size_t pafMaxDistancesLen = (size_t)PyList_Size(pafMaxDistances);
			
		std::vector<int> _strides(stridesLen);
		std::vector<std::string> _keypoints(keypointsLen);
		std::vector<std::tuple<int, int>> _skeleton(skeletonLen);
		std::vector<int> _pifIndices(pifIndicesLen);
		std::vector<int> _pafIndices(pafIndicesLen);
		std::vector<float> _pifMinScales(pifMinScalesLen);
		std::vector<float> _pafMinDistances(pafMinDistancesLen);
		std::vector<float> _pafMaxDistances(pafMaxDistancesLen);

		for (size_t i = 0; i < stridesLen; i++)
			_strides.at(i) = (int)PyLong_AsLong(PyList_GetItem(strides, i));

		for (size_t i = 0; i < keypointsLen; i++)
		{
			PyObject* item = PyList_GetItem(keypoints, i);

			// Check if item is string
			if (PyUnicode_Check(item))
			{
				PyObject* pyString = PyUnicode_AsEncodedString(item, "utf-8", "UNICODE STRING ITEM");
				const char *tmpCstChar =  PyBytes_AS_STRING(pyString);
				_keypoints.at(i) = std::string(tmpCstChar);
			}
		}

		for (size_t i = 0; i < skeletonLen; i++)
		{
			PyObject* item = PyList_GetItem(skeleton, i);

			if (PyTuple_Check(item))
			{
				const int tmpCstInt1 = (int)PyLong_AsLong(PyTuple_GetItem(item, 0));
				const int tmpCstInt2 = (int)PyLong_AsLong(PyTuple_GetItem(item, 1));
				_skeleton.at(i) = std::make_tuple(tmpCstInt1, tmpCstInt2);
			}
		}

		for (size_t i = 0; i < pifIndicesLen; i++)
			_pifIndices.at(i) = (int)PyLong_AsLong(PyList_GetItem(pifIndices, i));
		for (size_t i = 0; i < pafIndicesLen; i++)
			_pafIndices.at(i) = (int)PyLong_AsLong(PyList_GetItem(pafIndices, i));
		for (size_t i = 0; i < pifMinScalesLen; i++)
			_pifMinScales.at(i) = (int)PyLong_AsLong(PyList_GetItem(pifMinScales, i));
		for (size_t i = 0; i < pafMinDistancesLen; i++)
			_pafMinDistances.at(i) = (int)PyLong_AsLong(PyList_GetItem(pafMinDistances, i));
		for (size_t i = 0; i < pafMaxDistancesLen; i++)
			_pafMaxDistances.at(i) = (int)PyLong_AsLong(PyList_GetItem(pafMaxDistances, i));

		return new openpifpaf::PifPaf(_strides,
									  _keypoints,
									  _skeleton,
									  _pifIndices,
									  _pafIndices,
									  _pifMinScales,
									  _pafMinDistances,
									  _pafMaxDistances,
									  0.2f);
	}
}
